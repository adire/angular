// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC4NVrW39h0sY6H0_xSndA7ih9nk-gr4dE",
    authDomain: "hello-adire.firebaseapp.com",
    databaseURL: "https://hello-adire.firebaseio.com",
    projectId: "hello-adire",
    storageBucket: "hello-adire.appspot.com",
    messagingSenderId: "1053400274482",
    appId: "1:1053400274482:web:0dcc4c5ba16c2e0be9ce2b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
